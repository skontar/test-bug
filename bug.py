import inspect
import re
from collections import defaultdict
from itertools import chain
from statistics import mean, median

import requests
from django.core.management.base import BaseCommand
from django.db.models import Exists, Max, OuterRef
from requests_kerberos import HTTPKerberosAuth

import inspect
import re

"""
A

A
A $ A

A
A `A` A
* A
* A
* A
"""


class Command(BaseCommand):
    """
    A.
    """

    # ========================================= CONSTANTS ==========================================
    A = ["A", "A", "A", "A"]  # A

    """
    A A "A A":
    * AA
    * A "A A"
      - A
        A
    """

    a = list(
        sum(abc=Test("pk"))
        # AAA
        .filter(abc=list(Test_2))
        # AAA "AAA" AAA
        .exclude(abc=Test_3)
    )

    """
    A "A A":
    * A
      - A
      - A
    * A
      - A
        A
    """
    a = list(
        sum(abc=Test("pk"))
        # AAA
        .filter(abc=list(Test_2))
        # AAA "AAA" AAA
        .exclude(abc=Test_3)
    )

    """
    A "A A":
    * A 
    """
    a = list(
        sum(abc=Test("pk"))
        # AAA
        .filter(abc=list(Test_2))
        # AAA "AAA" AAA
        .exclude(abc=Test_3)
    )